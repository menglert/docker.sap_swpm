SAP Software Provisioning Manager Docker Docker Image
======
# Introduction
This Docker Image can run the SAP Software Provisioning Manager.
# Prepare
* Clone the repository
  * `git clone <Repository URL>`
* Download the required Binaries from SAP
  * SAPCAR
    * e.g. `51053671_8.ZIP` found in `SOFTWARE_PROVISIONING_MGR_1.0_SP25_/DATA_UNITS/SWPM10_IM_LINUX_X86_64/SAPCAR_1110`
  * SWPM
    * e.g. `51053671_8.ZIP` found in `SOFTWARE_PROVISIONING_MGR_1.0_SP25_/DATA_UNITS/SWPM10_IM_LINUX_X86_64/SWPM10SP25_02.SAR`
* Provide the files with the following names in the `docker` folder:
  * `sapcar.exe`
  * `swpm_1.sar` (or `swpm_2.sar` - has to be changed in the [Dockerfile])
# Build
Sample `docker build` command:
``` bash
$ cd docker
$ docker build \
-t <image-name>:latest .
```
# Run
Sample `docker run` command:
``` bash
$ docker run -d \
    --name <container-name> \
    -p <HOST-PORT>:4237 \
    <image-name>
```
# Access the UI
* Open the Browser
* Navigate to the [SWPM URL] (Change the `<HOST-PORT>`)
# Custom Configuration
Custom Configuration can be done via Environment variables e.g. during `docker run` specify `-e SAPINST_GUI_HOSTNAME=<MY-HOST-NAME>`. The possible configuration parameters are written to StdOut furing startup. You can check them by printing the logs `docker logs <container-name>`.

[Dockerfile]: /docker/Dockerfile
[SWPM URL]: https://localhost:<HOST-PORT>/sapinst/docs/index.html
